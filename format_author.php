<?php

function acronym($string) {
    $output = null;
    $token  = strtok($string, ' ');
    while ($token !== false) {
        $output .= $token[0];
        $token = strtok(' ');
    }
    return $output;
}

function format_authors($authors) {
  $authors_list = explode(" and ", $authors);
  $ret = "";

  $i = 0;
  foreach($authors_list as $author) {
    $a = explode(", ", $author);

    $ret .= acronym($a[1]);
    $ret .= ". ";
    $ret .= $a[0];

    if($i < sizeof($authors_list) - 2) {
      $ret .= ", ";
    }
    else if($i < sizeof($authors_list) - 1) {
      $ret .= " and ";
    }
    $i++;
  }

  return $ret;
}


function format_bib_item($item) {
  $ret = "";

  if(isset($item["author"]) and isset($item["title"])) {
    $ret .= "\t<li>\n";
    $ret .= "\t\t<span class='authors'>" . format_authors($item["author"]) . "</span>, \n";
    $ret .= "\t\t<span class='title'>" . $item["title"] . "</span>, \n";

    if(isset($item["journal"])) {
      if(isset($item["url"])) {
        $ret .= "\t\t<a href='" . $item["url"] . "'>\n\t";
      }
      $ret .= "\t\t<span class='journal'>" . $item["journal"] . "</span>, \n";
      if(isset($item["url"])) {
        $ret .= "\t\t</a>\n";
      }
    }
    if(isset($item["year"])) {
      $ret .= "\t\t<span class='year'>" . $item["year"] . "</span>.\n";
    }

    if(isset($item["pdf"])) {
      $ret .= "\t\t<a href='" . $item["pdf"] . "'>\n\t";
      $ret .= "\t\t<span class='link'>[pdf]</span> \n";
      $ret .= "\t\t</a> \n";
    }

    if(isset($item["arxiv"])) {
      $ret .= "\t\t<a href='" . $item["arxiv"] . "'>\n\t";
      $ret .= "\t\t<span class='link'>[arxiv]</span> \n";
      $ret .= "\t\t</a> \n";
    }

    if(isset($item["hal"])) {
      $ret .= "\t\t<a href='" . $item["hal"] . "'>\n\t";
      $ret .= "\t\t<span class='link'>[hal]</span> \n";
      $ret .= "\t\t</a> \n";
    }

    if(isset($item["poster"])) {
      $ret .= "\t\t<a href='" . $item["poster"] . "'>\n\t";
      $ret .= "\t\t<span class='link'>[poster]</span> \n";
      $ret .= "\t\t</a> \n";
    }

    if(isset($item["slides"])) {
      $ret .= "\t\t<a href='" . $item["slides"] . "'>\n\t";
      $ret .= "\t\t<span class='link'>[slides]</span> \n";
      $ret .= "\t\t</a> \n";
    }

    if(isset($item["video"])) {
      $ret .= "\t\t<a href='" . $item["video"] . "'>\n\t";
      $ret .= "\t\t<span class='link'>[video]</span> \n";
      $ret .= "\t\t</a> \n";
    }



    $ret .= "\t</li>\n";
  }
  return $ret;
}


?>