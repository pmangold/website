<p>
J'ai donné en 2020–2021 un cours de graphes et machine learning en L3 MIASHS.
Les ressources des cours sont ici, avec les slides et les TP :
</p>

<ul>
<li>
Cours 1
(<a href="course/graphes/TP1/Cours1-Presentation.pdf">slides</a>,
<a href="course/graphes/TP1/TP1 - Premiers graphes.ipynb" download>TP</a>) :
présentation du cours.
</li>

<li>
Cours 2
(<a href="course/graphes/TP2/Cours2-Graphes.pdf">slides</a>,
<a href="course/graphes/TP2/TP2 - Labyrinthe.ipynb" download>TP</a>) :
vocabulaire des graphes.
</li>

<li>
Cours 3
(<a href="course/graphes/TP3/Cours3-Arbres.pdf">slides</a>,
<a href="course/graphes/TP3/TP3 - Arbres MinMax.ipynb" download>TP</a>,
<a href="course/graphes/DM1/DM1 - Kruskal.ipynb">DM</a>) :
arbres.
</li>

<li>
Cours 4
(<a href="course/graphes/TP4/Cours4-pb-graphes.pdf">slides</a>,
<a href="course/graphes/TP4/TP4 - Cliques.ipynb" download>TP</a>) :
problèmes difficiles sur les graphes.
</li>
<li>
Cours 5
(<a href="course/graphes/TP5/Cours5-representation-matricielle.pdf">slides</a>,
<a href="course/graphes/TP5/TD1-matrices.pdf">TD</a>) :
représentation matricielle.
</li>

<li>
<a href="course/graphes/Interro/Interro.ipynb">Interro de mi-semestre</a>
et <a href="course/graphes/DM2/DM2 - Dijkstra.ipynb">DM</a>.
</li>

<li>
Cours 6
(<a href="course/graphes/TP6/Cours6-laplacien.pdf">slides</a>,
<a href="course/graphes/TP6/TP6 - Laplacien.ipynb" download>TP</a>):
matrice laplacienne.
</li>
<li>
Cours 7
(<a href="course/graphes/TP7/Cours7-arbres-decision.pdf">slides</a>,
<a href="course/graphes/TP7/TP7 - Arbres de Décision.ipynb" download>TP</a>) :
ML supervisé, arbres de décision.
</li>
<li>
Cours 8
(<a href="course/graphes/TP8/TP8 (Mini-Projet) - PageRank.ipynb">mini projet</a>) :
PageRank.
</li>
<li>
Cours 9
(<a href="course/graphes/TP9/Cours9-clustering.pdf">slides</a>,
<a href="course/graphes/TP9/TP9 - Clustering.ipynb" download>TP</a>) :
ML non supervisé, clustering.
</li>
<li>
Cours 10
(<a href="course/graphes/TP10/Cours10-semisupervise.pdf">slides</a>,
<a href="course/graphes/TP10/TP10 - Propagation de Labels.ipynb" download>TP</a>) :
ML semi-supervisé, propagation de label.
</li>
<li>
Cours 11
(<a href="course/graphes/TP11/TP11 - Machine Learning.ipynb" download>TP</a>) :
ML en pratique.
</li>
</ul>

<p>
Si pour un raison ou une autre, vous avez besoin de la correction, n'hésitez pas à me contacter.
</p>