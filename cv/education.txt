2020&ndash;now
Inria Lille
Thèse
Optimisation décentralisée et privée pour le machine learning

2018&ndash;2019
Université Lyon 1
Agrégation de mathématiques, option informatique

2017&ndash;2018
École Polytechnique, Télécom Paris
Master 2 Data Sciences

2016&ndash;2017
ENS de Lyon
Master 1 Informatique fondamentale

2015&ndash;2016
ENS de Lyon
Licence 3 Informatique fondamentale

2013&ndash;2015
Lycée du Parc
Prépa MPSI&ndash;MP*

2010&ndash;2013
Lycée Ampère
Baccalauréat Scientifique
