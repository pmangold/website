<?php
function parse_bib_file($path) {
$ref_items = array();
$i = -1;
$brace_count = 0;
$in_name = false;
$in_label = false;
$in_value = false;
$in_type = false;
$current_label = "";


$fed = file_get_contents($path);
$a = str_split($fed);

foreach($a as $c) {
  if(ctype_space($c) and ($in_value == false or $brace_count <= 1)) {
    continue;
  }

  // count braces
  if($c == '{') {
    $brace_count++;

    if($brace_count == 1 and $in_type == true) {
      $in_type = false;
      $in_name = true;
      $ref_items[$i]["name"] = "";
    }

    continue;
  }
  if($c == '}') {
    $brace_count--;

    if($brace_count == 0) {
      $in_name = false;
      $in_label = false;
      $in_value = false;
    }

    continue;
  }

  // keep track of current semantic position
  if($c == "@" and $brace_count == 0) {
    $in_type = true;
    $i++;
    $ref_items[$i] = array();
  }

  if($in_name == true and $c == ',') {
    $in_name = false;
    $in_label = true;
    $current_label = "";

    continue;
  }

  if($in_label == true and $c == "=") {
    $in_label = false;
    $in_value = true;

    $ref_items[$i][$current_label] = "";

    continue;
  }

  if($in_value == true and $c == "," and $brace_count <= 1) {
    $in_value = false;
    $in_label = true;
    $current_label = "";

    continue;
  }



  if($in_name == true) {
    $ref_items[$i]["name"] .= $c;
  }

  if($in_label == true) {
    $current_label .= $c;
  }

  if($in_value == true) {
    $ref_items[$i][$current_label] .= $c;
  }


}

return $ref_items;
}
