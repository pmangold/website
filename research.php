<?php

$page_id = "research";
$pages = ["summary", "publications"];

ini_set( 'error_reporting', E_ALL );
ini_set( 'display_errors', true );
 include './head.html'; 
?>
<?php
if(isset($_GET["page"])) {
  $page = $_GET["page"];
}
else {
  $page = "publications";
}

if(isset($_GET["topic"])) {
  $topic = $_GET["topic"];
}
else {
  $topic = "none";
}    
?>
<nav class="subnav">
  <ul>
  <?php
    foreach($pages as $page_name) {
        if($page_name == $page) {
            echo "\t<li class='current'><a href='research.php?page=" . $page_name . "'>" . $page_name . "</a></li>\n";
	}
	else {
            echo "\t<li><a href='research.php?page=" . $page_name . "'>" . $page_name . "</a></li>\n";
	}
    }
//    <li><a href="">Summary</a></li>
//    <li><a href="">Publications</a></li>
//    <li><a href="">Readings</a></li>
//
  ?>
  </ul>
</nav>

<?php
  foreach($pages as $page_name) {
    if($page == $page_name) {
      include './research_' . $page_name . ".html";
    }   
  }
?>

<?php include 'foot.html'; ?>
